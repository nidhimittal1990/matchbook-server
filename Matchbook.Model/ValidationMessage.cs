﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchbook.Model
{
    public class ValidationMessage
    {
        public MessageType MessageType { get; set; }
        public string Message { get; set; }
    }

    public enum MessageType
    {
        Error,
        Info,
        Warning
    }
}
