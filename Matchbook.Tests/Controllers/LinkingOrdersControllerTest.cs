﻿using Matchbook.Db;
using Matchbook.Domain.Interfaces;
using Matchbook.Model;
using Matchbook.Tests.Persistence;
using Matchbook.WebHost.Controllers;
using MatchBook.Domain.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Microsoft.Data.SqlClient;
using System.Runtime.Serialization;
using AutoFixture;

namespace Matchbook.Tests.Controllers
{
    public class LinkingOrdersControllerTest
    {
        private Mock<IOrderLinkingRequestValidator> _validator;
        private Mock<ILinkingOrderManager> _manager;
        private LinkingOrdersController _controller;
        private Fixture _fixture;
        public LinkingOrdersControllerTest()
        {
            _fixture = new Fixture();
            _validator = new Mock<IOrderLinkingRequestValidator>();
            _manager = new Mock<ILinkingOrderManager>();
            _controller = new LinkingOrdersController(_validator.Object, _manager.Object);
        }

        [Fact]
        public void LinkingOrders_ThrowsException_ReturnsInternalServerError()
        {
            int orderId1 = -1;
            int orderId2 = 3;
            List<ValidationMessage> message = null;
            _validator.Setup(x => x.ValidateRequest(It.IsAny<int>(), It.IsAny<int>())).Returns(message);
            _manager.Setup(x => x.LinkOrders(It.IsAny<int>(), It.IsAny<int>()))
               .Throws<Exception>();
            var response = _controller.OrderLinking(orderId1, orderId2);
            var result = response as ObjectResult;
            result.StatusCode.ShouldBe((int)HttpStatusCode.InternalServerError);
        }

        [Fact]
        public void failed_validation_returns_bad_request()
        {
            int orderId1 = -1;
            int orderId2 = 3;
            List<ValidationMessage> validationMessages = _fixture.Create<List<ValidationMessage>>();
            _validator.Setup(x => x.ValidateRequest(It.IsAny<int>(), It.IsAny<int>())).Returns(validationMessages);
            var response = _controller.OrderLinking(orderId1, orderId2);
            response.ShouldBeOfType(typeof(BadRequestObjectResult));
            var result = response as BadRequestObjectResult;
            result.StatusCode.HasValue.ShouldBeTrue();
            result.StatusCode.Value.ShouldBe((int)HttpStatusCode.BadRequest);
        }
    }
}
