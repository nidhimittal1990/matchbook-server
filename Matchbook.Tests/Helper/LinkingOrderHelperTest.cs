﻿using AutoFixture;
using Matchbook.Db;
using Matchbook.Model;
using Matchbook.Tests.Persistence;
using MatchBook.Domain.Helper;
using Microsoft.EntityFrameworkCore;
using Moq;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Matchbook.Tests.Helper
{
    public class LinkingOrderHelperTest
    {
        private Mock<MatchbookDbContext> _dbContext;
        private LinkingOrderHelper _helper;
        private Fixture _fixture;

        public LinkingOrderHelperTest()
        {
            _fixture = new Fixture();
        }

        [Fact]
        public void given_same_subaccountId_and_productsymbol_return_true()
        {
            int order1 = 1;
            int order2 = 3;
            var orders = new List<Order>
            {
                new Order { Id = 1,ProductSymbol = "abc",SubAccountId = 2 },
                new Order { Id = 2,ProductSymbol = "abc",SubAccountId = 2 },
                new Order { Id = 3,ProductSymbol = "123",SubAccountId = 4},
            }.AsQueryable();

          //  var mockSet = new Mock<DbSet<Order>>();
           // mockSet.As<IQueryable<Order>>().Setup().Returns(data.OR);
         //   var _dbContext = new MatchbookDbContext { Orders = data};
          //  _dbContext.Setup(m => m.Orders).Returns(mockSet.Object);
            _helper = new LinkingOrderHelper(_dbContext.Object);
            bool result = _helper.AreOrdersCompatible(order1, order2);
        }
    }
}
