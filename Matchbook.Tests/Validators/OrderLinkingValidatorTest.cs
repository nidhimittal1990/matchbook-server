﻿using AutoFixture;
using Matchbook.Domain.Interfaces;
using Matchbook.Domain.Validatiors;
using Matchbook.Model;
using Matchbook.Tests.Persistence;
using MatchBook.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Matchbook.Tests.Validators
{
    public class OrderLinkingValidatorTest
    {
        private Fixture _fixture;
        private IOrderLinkingRequestValidator _requestValidator;
        private Mock<ILinkingOrderHelper> _helper;

        public OrderLinkingValidatorTest()
        {
            _helper = new Mock<ILinkingOrderHelper>();
            _requestValidator = new OrderLinkingRequestValidator(_helper.Object);
        }

        [Fact]
        public void input_orderId1_should_be_positive()
        {
            int orderId1 = -1;
            int orderId2 = 3;
            var expextedValidationResult = new List<ValidationMessage> { new ValidationMessage { MessageType = MessageType.Error, Message = "Order1 is not valid" } };
            var response = _requestValidator.ValidateRequest(orderId1,orderId2);
            CollectionAssert.AreEqual(expextedValidationResult.Select(x=>x.Message).ToList(), response.Select(x=>x.Message).ToList());
            CollectionAssert.AreEqual(expextedValidationResult.Select(x => x.MessageType).ToList(), response.Select(x => x.MessageType).ToList());
        }
        [Fact]
        public void input_orderId2_should_be_positive()
        {
            int orderId1 = 1;
            int orderId2 = -3;
            var expextedValidationResult = new List<ValidationMessage> { new ValidationMessage { MessageType = MessageType.Error, Message = "Order2 is not valid" } };
            var response = _requestValidator.ValidateRequest(orderId1, orderId2);
            CollectionAssert.AreEqual(expextedValidationResult.Select(x => x.Message).ToList(), response.Select(x => x.Message).ToList());
            CollectionAssert.AreEqual(expextedValidationResult.Select(x => x.MessageType).ToList(), response.Select(x => x.MessageType).ToList());
        }
        [Fact]
        public void input_orderId1_and_orderId2_should_be_positive()
        {
            int orderId1 = -1;
            int orderId2 = -3;
            var expextedValidationResult = new List<ValidationMessage> { new ValidationMessage { MessageType = MessageType.Error, Message = "Order1 is not valid" }, new ValidationMessage { MessageType = MessageType.Error, Message = "Order2 is not valid" } };
            var response = _requestValidator.ValidateRequest(orderId1, orderId2);
            CollectionAssert.AreEqual(expextedValidationResult.Select(x => x.Message).ToList(), response.Select(x => x.Message).ToList());
            CollectionAssert.AreEqual(expextedValidationResult.Select(x => x.MessageType).ToList(), response.Select(x => x.MessageType).ToList());
        }

        [Fact]
        public void given_orders_When_not_compatible_returns_message()
        {
            int orderId1 = 1;
            int orderId2 = 3;
            _helper.Setup(x => x.AreOrdersCompatible(It.IsAny<int>(), It.IsAny<int>())).Returns(false);
            var expextedValidationResult = new List<ValidationMessage> { new ValidationMessage { MessageType = MessageType.Error, Message = "Given orders can not be linked" } };
            var response = _requestValidator.ValidateRequest(orderId1, orderId2);
            CollectionAssert.AreEqual(expextedValidationResult.Select(x => x.Message).ToList(), response.Select(x => x.Message).ToList());
            CollectionAssert.AreEqual(expextedValidationResult.Select(x => x.MessageType).ToList(), response.Select(x => x.MessageType).ToList());
        }

        [Fact]
        public void given_orders_When_already_linked_returns_message()
        {
            int orderId1 = 1;
            int orderId2 = 3;
            _helper.Setup(x => x.AreOrdersCompatible(It.IsAny<int>(), It.IsAny<int>())).Returns(true);
            _helper.Setup(x => x.IsAlreadyLinked(It.IsAny<int>())).Returns(true);
            var expextedValidationResult = new List<ValidationMessage> { new ValidationMessage { MessageType = MessageType.Error, Message = "Order1 is already linked" }, new ValidationMessage { MessageType = MessageType.Error, Message = "Order2 is already linked" } };
            var response = _requestValidator.ValidateRequest(orderId1, orderId2);
            CollectionAssert.AreEqual(expextedValidationResult.Select(x => x.Message).ToList(), response.Select(x => x.Message).ToList());
            CollectionAssert.AreEqual(expextedValidationResult.Select(x => x.MessageType).ToList(), response.Select(x => x.MessageType).ToList());
        }

    }
}
