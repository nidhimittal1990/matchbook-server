﻿using Matchbook.Db;
using Matchbook.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MatchBook.Domain.Interfaces;
using Matchbook.Domain.Interfaces;
using Microsoft.Data.SqlClient;

namespace Matchbook.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LinkingOrdersController : ControllerBase
    {
        private readonly IOrderLinkingRequestValidator _validator;
        private readonly ILinkingOrderManager _linkingOrderManager;
        public LinkingOrdersController(IOrderLinkingRequestValidator validator, ILinkingOrderManager linkingOrderManager)
        {
            _validator = validator;
            _linkingOrderManager = linkingOrderManager;
        }

        [HttpPost]
        public ActionResult OrderLinking(int orderId1, int orderId2)
        {
            List<ValidationMessage> validationResult = null;
            try
            {
                validationResult = _validator.ValidateRequest(orderId1, orderId2);
                if (validationResult.Any())
                {
                    return BadRequest(validationResult.Select(x => x.Message));
                }
                var result =  _linkingOrderManager.LinkOrders(orderId1, orderId2);
                return StatusCode((int)HttpStatusCode.Created, result);
            }
            catch(SqlException sqlEx)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed, sqlEx.Message);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
