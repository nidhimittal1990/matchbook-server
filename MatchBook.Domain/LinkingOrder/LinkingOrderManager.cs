﻿using Matchbook.Db;
using Matchbook.Model;
using MatchBook.Domain.Interfaces;
using System;
using System.Linq;

namespace MatchBook.Domain.LinkingOrder
{
    public class LinkingOrderManager : ILinkingOrderManager
    {
        private readonly ILinkingOrderHelper _helper;
        private readonly MatchbookDbContext _dbContext;
        public LinkingOrderManager(MatchbookDbContext dbContext, ILinkingOrderHelper helper)
        {
            _dbContext = dbContext;
            _helper = helper;
        }
        public int LinkOrders(int order1, int order2)
        {
            return _helper.LinkGivenOrders(order1, order2);
        }
    }
}
