﻿using Matchbook.Domain.Interfaces;
using Matchbook.Model;
using MatchBook.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Matchbook.Domain.Validatiors
{
    public class OrderLinkingRequestValidator : IOrderLinkingRequestValidator
    {
        private readonly ILinkingOrderHelper _helper;
        public OrderLinkingRequestValidator(ILinkingOrderHelper helper)
        {
            _helper = helper;
        }
        public List<ValidationMessage> ValidateRequest(int orderId1, int orderId2)
        {
            List<ValidationMessage> messages = new List<ValidationMessage>();
            if (orderId1 <= 0 || orderId2 <= 0)
            {
                if (orderId1 <= 0)
                {
                    messages.Add(new ValidationMessage() { Message = "Order1 is not valid", MessageType = MessageType.Error });
                }
                if (orderId2 <= 0)
                {
                    messages.Add(new ValidationMessage() { Message = "Order2 is not valid", MessageType = MessageType.Error });
                }
            }
            else if (!_helper.AreOrdersCompatible(orderId1, orderId2))
            {
                messages.Add(new ValidationMessage { MessageType = MessageType.Error, Message = "Given orders can not be linked" });
            }
            else
            {
                if (_helper.IsAlreadyLinked(orderId1))
                {
                    messages.Add(new ValidationMessage { MessageType = MessageType.Error, Message = "Order1 is already linked" });
                }
                if (_helper.IsAlreadyLinked(orderId2))
                {
                    messages.Add(new ValidationMessage { MessageType = MessageType.Error, Message = "Order2 is already linked" });
                }
            }
            return messages;
        }


    }
}
