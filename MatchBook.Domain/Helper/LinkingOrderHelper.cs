﻿using Matchbook.Db;
using Matchbook.Model;
using MatchBook.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace MatchBook.Domain.Helper
{
    public class LinkingOrderHelper : ILinkingOrderHelper
    {
        private readonly MatchbookDbContext _dbContext;
        public LinkingOrderHelper(MatchbookDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        private Order GetOrderDetails(int orderId)
        {
           return _dbContext.Orders.Where(x => x.Id == orderId).Select(o => new Order{Id= o.Id, ProductSymbol = o.ProductSymbol,SubAccount = o.SubAccount }).FirstOrDefault();
        }

        public bool AreOrdersCompatible(int order1, int order2)
        {
            var order1Details = GetOrderDetails(order1);
            var order2Details = GetOrderDetails(order2);
            return order1Details.SubAccountId == order2Details.SubAccountId && order1Details.ProductSymbol == order2Details.ProductSymbol ? true : false;
        }

        public bool IsAlreadyLinked(int orderId)
        {
           var order =  (from linkedOrder in _dbContext.OrderLink.Select(x => x.LinkedOrders)
             from o in linkedOrder
             where o.Id == orderId
             select o).FirstOrDefault();
            return order != null ? true : false;
        }

        public int LinkGivenOrders(int order1, int order2)
        {
            try
            {
                var orderLink = new OrderLink { Name = Guid.NewGuid().ToString() };
                _dbContext.OrderLink.Add(orderLink);
                _dbContext.SaveChanges();

                int linkId = _dbContext.OrderLink.Max(ol => ol.Id);
                var o1 = _dbContext.Orders.First(x => x.Id == order1);
                o1.LinkId = linkId;
                var o2 = _dbContext.Orders.First(x => x.Id == order2);
                o2.LinkId = linkId;

                _dbContext.SaveChanges();
                return linkId;

                //ICollection<Order> orders = new List<Order>();
                //orders.Add(GetOrderDetails(order1));
                //orders.Add(GetOrderDetails(order2));
                //_var orderLink = new OrderLink { Name = Guid.NewGuid().ToString() };
                //_dbContext.OrderLink.Attach(orderLink);
                //_dbContext.Entry(orderLink).Property("LinkedOrders").IsModified = true;
                // _dbContext.SaveChanges();
                //return _dbContext.OrderLink.Max(ol => ol.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
