﻿using Matchbook.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace MatchBook.Domain.Interfaces
{
    public interface ILinkingOrderHelper
    {
        bool AreOrdersCompatible(int order1, int order2);
        bool IsAlreadyLinked(int order);
        int LinkGivenOrders(int order1, int order2);
    }
}
