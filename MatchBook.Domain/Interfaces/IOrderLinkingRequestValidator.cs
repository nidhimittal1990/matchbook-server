﻿using Matchbook.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Matchbook.Domain.Interfaces
{
    public interface IOrderLinkingRequestValidator
    {
        List<ValidationMessage> ValidateRequest(int orderId1, int orderId2);
    }
}
