﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatchBook.Domain.Interfaces
{
    public interface ILinkingOrderManager
    {
        int LinkOrders(int order1, int order2);
    }
}
